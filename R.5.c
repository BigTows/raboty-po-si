#include "stdafx.h"
#include <windows.h>
#include <strsafe.h>
#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <ctype.h>
#define _CRT_SECURE_NO_WARNINGS

struct Node {
	HANDLE hFind;
	TCHAR Path[1024];
	struct Node *next;
};

typedef struct Node *PNode;
PNode List = (PNode)malloc(sizeof(struct Node));
PNode List_2 = List;
int Thread_Counter;
void printE(CHAR * Message) {
	printf("%d: ", GetLastError());
	LPVOID MsgBuffer;
	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT),
		(LPTSTR)&MsgBuffer,
		0,
		NULL);
	_tprintf((LPTSTR)MsgBuffer);
	if (strlen(Message) != 0)
		printf(": %s", Message);
	LocalFree(MsgBuffer);
}

TCHAR * getNum(TCHAR Test[1024]) {
	if (_tcslen(Test) == 1) return _T("100");
	else if (_tcslen(Test) == 2) return _T("10");
	else {
		return _T("1");
	}
}

int isGoodFile(TCHAR Test[1024]) {
	for (int i = 0; i<_tcslen(Test); i++) {
		if (Test[i]>57 && Test[i]<48) {
			return 0;
		}
	}
	return 1 && _tcslen(Test) <= 3;
}


DWORD WINAPI CheckDir(LPVOID Path) {
	WIN32_FIND_DATA FileData;
	HANDLE hFind;
	LPVOID Def_Dir = Path;
	TCHAR Test_Path[1024] = _T("");
	TCHAR New_Path[1024] = _T("");
	TCHAR New_Folder[1024] = _T("");
	hFind = FindFirstFile((LPTSTR)Def_Dir, &FileData);
	if (hFind == INVALID_HANDLE_VALUE)
	{
		printE("");
		return 0;
	}
	while (FindNextFile(hFind, &FileData)) {
		if (!(FileData.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY) && !(FileData.dwFileAttributes&FILE_ATTRIBUTE_DEVICE)) {
			if (isGoodFile(FileData.cFileName)) {
				TCHAR path2[1024] = _T("");
				TCHAR path3[1024] = _T("");
				_tcscat(path2, (LPTSTR)Def_Dir);
				//remove *, last char
				path2[_tcslen(path2) - 1] = 0;
				_tcscat(path3, path2);

				_tcscat(path2, getNum(FileData.cFileName));
				_tcscat(path3, FileData.cFileName);
				_tcscat(path2, FileData.cFileName);
				MoveFile(path3, path2);
			}
			if (GetLastError() != ERROR_NO_MORE_FILES) printE("");

		}
		else
		{
			if ((FileData.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY) && !(FileData.dwFileAttributes&FILE_ATTRIBUTE_DEVICE)) {
				if (!((_tccmp(FileData.cFileName, _T("..")) == 0) || (_tccmp(FileData.cFileName, _T(".")) == 0))) {
					List->next = (PNode)malloc(sizeof(struct Node));
					List = List->next;
					if (List == NULL) {
						printf("Memory FULL");
						return -1;
					}
				
					_tcscpy(List->Path, (LPTSTR)Def_Dir);
					List->Path[_tcslen(List->Path) - 1] = 0;
					_tcscat(List->Path, FileData.cFileName);
					_tcscat(List->Path, _T("\\*"));
					List->hFind = CreateThread(NULL, 0, CheckDir, List->Path, 0, NULL);
					Thread_Counter++;
					printE("");
				}
			}
		}
	}
	List = List_2;
	for (int i = 0; i < Thread_Counter; i++) {
		WaitForSingleObject(List_2->hFind, INFINITE);
		List_2 = List_2->next;
		free(List);
		List = List_2;
		printE("");
	}
	return 1;
}

int main()
{
	CheckDir(_T("C:\\Test\\*"));
	system("Pause");
	return 0;
}

