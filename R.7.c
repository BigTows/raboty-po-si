#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

struct stats{
    int count;
};
struct stats Counter;
struct stat st;
void ScanDir(char Path [1024]){
    DIR * dir = opendir(Path);
    struct dirent * file;
    while ((file=readdir(dir))!=NULL){
        if (file->d_type==DT_REG){
            char test[1024]="";
            strcpy(test,Path);
            strcat(test, file->d_name);
            stat(test, &st);
            if (st.st_size>1024){
                printf("%s\n",test);
                Counter.count++;
            }
        }else if (file->d_type==DT_DIR && (strcmp(file->d_name,".")!=0)
                  && (strcmp(file->d_name, "..")!=0)){
            char path2[1024] ="";
            strcat(path2,Path);
            
            strcat(path2,file->d_name);
            strcat(path2,"/");
            ScanDir(path2);
        }
    }
}

int main() {
    Counter.count=0;
    char path [1024] ="/Users/bigtows/Desktop/Test/";
    ScanDir(path);
    printf("%d",Counter.count);
    return 0;
}