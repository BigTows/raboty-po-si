#define _CRT_SECURE_NO_WARNINGS
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <tchar.h>

void printError() {

	printf("%d: ", GetLastError());
	LPVOID MsgBuffer;
	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT),
		(LPTSTR)&MsgBuffer,
		0,
		NULL);
	_tprintf((LPTSTR)MsgBuffer);

	LocalFree(MsgBuffer);
}

void DeleteWords(TCHAR * Name) {
	TCHAR NewName[MAX_PATH] = _T("");
	
	Name[_tclen(Name) - 1] = 0;
	

}


int main()
{

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	TCHAR Path[1024] = _T("C:\\Test\\*");
	wchar_t wtext[1024];
	BOOL Succesful;
	int GoodhFind = 1;
	hFind = FindFirstFile(Path, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE)
	{
		GoodhFind = 0;
		if (GetLastError() != ERROR_FILE_NOT_FOUND) {
			printError();

		}
	}
	else
	{
		while (FindNextFile(hFind, &FindFileData)) {
			if (!(FindFileData.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY) && !(FindFileData.dwFileAttributes&FILE_ATTRIBUTE_DEVICE)) {
			
					TCHAR path2[1024] = _T("");
					TCHAR path3[1024] = _T("");
					TCHAR fileName[1024] = _T("");
					_tcscat(path2, Path);
					//remove *, last char
					path2[_tcslen(path2) - 1] = 0;
					_tcscat(path3, path2);
					_tcscat(path3, FindFileData.cFileName);
				
					_tcscat(fileName, FindFileData.cFileName);

					TCHAR t[MAX_PATH] = _T("");
					_tcscat(t, FindFileData.cFileName);
					if (!_tcscmp(&t[_tcslen(t) - 1], _T("~"))) {
						t[_tcslen(t) - 1] = 0;
					}
					_tcscat(path2, t);
				
				
					MoveFile(path3, path2);
				
				if (GetLastError() != ERROR_NO_MORE_FILES) printError();
			}
		

		}
	}
	if (GoodhFind == 1)
		if (!FindClose(hFind)) printError();
	system("PAUSE");
	return 0;
}