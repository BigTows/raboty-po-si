#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int is_regular_file(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return !S_ISREG(path_stat.st_mode);
}
int main (int count, char * arg[]){
	if (count!=2){
		printf("Wrong count params\n");
	}else{
		printf("%d\n",is_regular_file(arg[1]));
	}
	return 0;
}